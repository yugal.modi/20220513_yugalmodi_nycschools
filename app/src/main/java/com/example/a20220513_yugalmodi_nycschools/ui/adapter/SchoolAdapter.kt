package com.example.a20220513_yugalmodi_nycschools.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.a20220513_yugalmodi_nycschools.R
import com.example.a20220513_yugalmodi_nycschools.ui.model.SchoolUI
import com.example.a20220513_yugalmodi_nycschools.ui.view.ItemClickHandler

class SchoolAdapter(private val clickHandler: ItemClickHandler):ListAdapter<SchoolUI, ViewHolderSchool>(DiffCallBack) {
    object DiffCallBack: DiffUtil.ItemCallback<SchoolUI>(){
        override fun areItemsTheSame(oldItem: SchoolUI, newItem: SchoolUI): Boolean {
            return oldItem.dbn == newItem.dbn
        }

        override fun areContentsTheSame(oldItem: SchoolUI, newItem: SchoolUI): Boolean {
            return oldItem.dbn == newItem.dbn
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderSchool {
        return ViewHolderSchool(
            LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false),
            clickHandler
        )
    }

    override fun onBindViewHolder(holder: ViewHolderSchool, position: Int) {
        holder.bind((getItem((position))))
    }
}