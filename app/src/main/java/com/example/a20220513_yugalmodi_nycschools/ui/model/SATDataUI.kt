package com.example.a20220513_yugalmodi_nycschools.ui.model

data class SATDataUI (
    val dbn:String,
    val sat_critical_reading_avg_score:String,
    val sat_math_avg_score:String,
    val sat_writing_avg_score:String
)