package com.example.a20220513_yugalmodi_nycschools.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.a20220513_yugalmodi_nycschools.R
import com.example.a20220513_yugalmodi_nycschools.middleware.viewmodel.SATViewModel
import com.example.a20220513_yugalmodi_nycschools.ui.model.SATDataUI
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailedFragment:Fragment() {

    lateinit var textMath:TextView
    lateinit var textRead:TextView
    lateinit var textWrite:TextView

    lateinit var viewModel:SATViewModel

    private val bundle : DetailedFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val textSchoolName = view.findViewById<TextView>(R.id.text_school_name)
        val textOverview = view.findViewById<TextView>(R.id.text_overview)
        textSchoolName.text = bundle.bundleData.school_name
        textOverview.text = bundle.bundleData.overview_paragraph

        textMath = view.findViewById<TextView>(R.id.text_math)
        textRead = view.findViewById<TextView>(R.id.text_read)
        textWrite = view.findViewById<TextView>(R.id.text_write)

        viewModel = ViewModelProvider(this)[SATViewModel::class.java]
        viewModel.SATList.observe(viewLifecycleOwner){
            setSatData(it)
        }
        viewModel.getSatDetails(bundle.bundleData.dbn)
    }

    private fun setSatData(data:List<SATDataUI>){
        if(data.isNotEmpty()){
            textMath.text = data[0].sat_math_avg_score
            textRead.text = data[0].sat_critical_reading_avg_score
            textWrite.text = data[0].sat_writing_avg_score
        }
    }
}