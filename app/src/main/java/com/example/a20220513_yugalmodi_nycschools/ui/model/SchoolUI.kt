package com.example.a20220513_yugalmodi_nycschools.ui.model

import java.io.Serializable

data class SchoolUI (
    val dbn:String,
    val school_name:String,
    val overview_paragraph:String,
    val phone_number:String,
    val website:String,
    val latitude:Double,
    val longitude:Double
) : Serializable