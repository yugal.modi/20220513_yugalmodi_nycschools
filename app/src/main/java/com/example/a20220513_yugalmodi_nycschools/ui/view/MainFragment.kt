package com.example.a20220513_yugalmodi_nycschools.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220513_yugalmodi_nycschools.R
import com.example.a20220513_yugalmodi_nycschools.middleware.viewmodel.SchoolViewModel
import com.example.a20220513_yugalmodi_nycschools.ui.adapter.SchoolAdapter
import com.example.a20220513_yugalmodi_nycschools.ui.model.SchoolUI
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment:Fragment(), ItemClickHandler{

    lateinit var adapter: SchoolAdapter
    lateinit var viewModel:SchoolViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recycler = view.findViewById<RecyclerView>(R.id.recyclerview)
        val progressbar = view.findViewById<ProgressBar>(R.id.progress_bar)
        progressbar.visibility = View.VISIBLE

        //setAdapter
        adapter = SchoolAdapter(this)
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(requireContext())

        viewModel = ViewModelProvider(this)[SchoolViewModel::class.java]
        viewModel.schoolList.observe(viewLifecycleOwner){
            progressbar.visibility = View.GONE
            adapter.submitList(it)
        }
        viewModel.getAllSchool()
    }

    override fun onItemClick(item: SchoolUI) {
        findNavController().navigate(MainFragmentDirections.navActionMainFrag(item))
    }
}

interface ItemClickHandler{
    fun onItemClick(item:SchoolUI)
}