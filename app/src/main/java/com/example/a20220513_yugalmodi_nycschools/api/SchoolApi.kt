package com.example.a20220513_yugalmodi_nycschools.api

import com.example.a20220513_yugalmodi_nycschools.api.model.SATData
import com.example.a20220513_yugalmodi_nycschools.api.model.School
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApi {
    @GET("s3k6-pzi2.json")
    suspend fun getAllSchool():List<School>

    @GET("f9bf-2cp4.json")
    suspend fun getSATData(@Query("dbn") dbn:String):List<SATData>
}