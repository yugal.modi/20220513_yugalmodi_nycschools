package com.example.a20220513_yugalmodi_nycschools.api.DI

import com.example.a20220513_yugalmodi_nycschools.api.RepoSchool
import com.example.a20220513_yugalmodi_nycschools.api.SchoolApi
import com.example.a20220513_yugalmodi_nycschools.api.ServiceSchool
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


const val BASE_URL = "https://data.cityofnewyork.us/resource/"

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit():Retrofit.Builder{
        return Retrofit.Builder().baseUrl(BASE_URL).
            addConverterFactory(GsonConverterFactory.create())
    }

    @Singleton
    @Provides
    fun provideSchoolApi(retrofit: Retrofit.Builder):SchoolApi{
        return retrofit.build().create(SchoolApi::class.java)
    }

    @Singleton
    @Provides
    fun provideRepoSchool(api:SchoolApi):RepoSchool{
        return ServiceSchool(api);
    }

}