package com.example.a20220513_yugalmodi_nycschools.api.model

data class SATData (
    val dbn:String?,
    val sat_critical_reading_avg_score:String?,
    val sat_math_avg_score:String?,
    val sat_writing_avg_score:String?
)