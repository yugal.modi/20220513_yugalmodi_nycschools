package com.example.a20220513_yugalmodi_nycschools.api

import com.example.a20220513_yugalmodi_nycschools.api.model.SATData
import com.example.a20220513_yugalmodi_nycschools.api.model.School
import kotlinx.coroutines.flow.Flow

interface RepoSchool {
    fun getAllSchool():Flow<List<School>>
    fun getSatDetails(dbn:String):Flow<List<SATData>>
}