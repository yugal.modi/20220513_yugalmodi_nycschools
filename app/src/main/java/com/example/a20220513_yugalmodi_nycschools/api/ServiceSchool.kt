package com.example.a20220513_yugalmodi_nycschools.api

import com.example.a20220513_yugalmodi_nycschools.api.model.SATData
import com.example.a20220513_yugalmodi_nycschools.api.model.School
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ServiceSchool @Inject constructor(private val api:SchoolApi):RepoSchool{

    override fun getAllSchool(): Flow<List<School>> {
        return flow { emit(api.getAllSchool()) }
    }

    override fun getSatDetails(dbn:String): Flow<List<SATData>> {
        return flow { emit(api.getSATData(dbn)) }
    }

}