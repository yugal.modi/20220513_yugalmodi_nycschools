package com.example.a20220513_yugalmodi_nycschools.api.model

data class School (
    val dbn:String?,
    val school_name:String?,
    val overview_paragraph:String?,
    val phone_number:String?,
    val website:String?,
    val latitude:String?,
    val longitude:String?
)