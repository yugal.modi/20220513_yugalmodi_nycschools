package com.example.a20220513_yugalmodi_nycschools.middleware.mapper

import com.example.a20220513_yugalmodi_nycschools.api.model.SATData
import com.example.a20220513_yugalmodi_nycschools.api.model.School
import com.example.a20220513_yugalmodi_nycschools.ui.model.SATDataUI
import com.example.a20220513_yugalmodi_nycschools.ui.model.SchoolUI

class SATDataMapper {
    companion object{
        val map:((SATData)->(SATDataUI))={
            SATDataUI(
                it.dbn?:"",
                it.sat_critical_reading_avg_score?:"",
                it.sat_math_avg_score?:"",
                it.sat_math_avg_score?:""
            )
        }
    }
}