package com.example.a20220513_yugalmodi_nycschools.middleware.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220513_yugalmodi_nycschools.api.RepoSchool
import com.example.a20220513_yugalmodi_nycschools.middleware.mapper.SATDataMapper
import com.example.a20220513_yugalmodi_nycschools.middleware.mapper.SchoolMapper
import com.example.a20220513_yugalmodi_nycschools.ui.model.SATDataUI
import com.example.a20220513_yugalmodi_nycschools.ui.model.SchoolUI
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SATViewModel @Inject constructor(private val repo: RepoSchool): ViewModel() {
    val SATList = MutableLiveData<List<SATDataUI>>()
    fun getSatDetails(dbn:String){
        viewModelScope.launch {
            repo.getSatDetails(dbn).flowOn(Dispatchers.IO).catch {
                it.localizedMessage
            }.collect{
                    t-> SATList.postValue(t.map{ SATDataMapper.map.invoke(it)})
            }
        }
    }
}