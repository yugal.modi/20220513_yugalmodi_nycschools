package com.example.a20220513_yugalmodi_nycschools.middleware.mapper

import com.example.a20220513_yugalmodi_nycschools.api.model.School
import com.example.a20220513_yugalmodi_nycschools.ui.model.SchoolUI

class SchoolMapper {
    companion object{
        val map:((School)->(SchoolUI))={
            SchoolUI(
                it.dbn?:"",
                it.school_name?:"",
                it.overview_paragraph?:"",
                it.phone_number?:"",
                it.website?:"",
                if(it.latitude==null) 0.0 else it.latitude.toDouble(),
                if(it.longitude==null) 0.0 else it.longitude.toDouble()
            )
        }
    }
}